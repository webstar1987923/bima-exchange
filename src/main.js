import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

import 'bootstrap-sass/assets/javascripts/bootstrap'
//import the vue instance
import Vue from 'vue'
//import the App component
import App from './App'
//import the vue router
import VueRouter from 'vue-router'

import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

import VModal from 'vue-js-modal'
Vue.use(VModal, {
    dialog: true
})

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDq6UI1FsVcF8bNrB1yJuZwdeBmEnVcbRM',
        libraries: 'places, drawing, visualization'
    }
})

import VueNumeric from 'vue-numeric'
Vue.use(VueNumeric);

import {
    store
} from './store/';

//tell vue to use the router
Vue.use(VueRouter)
    /* eslint-disable no-new */

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);


// import { AccordionMenu } from 'vue-accordion-menu'
// Vue.component('AccordionMenu', AccordionMenu)

import lodash from 'lodash';
Object.defineProperty(Vue.prototype, '$lodash', {
    value: lodash
});

import moment from 'moment';
Object.defineProperty(Vue.prototype, '$moment', {
    value: moment
});

import Home from './components/Home'
import HowItWorks from './components/How-it-works'
import OurProviders from './components/Our-providers'
import AboutUs from './components/About-us'
import Faq from './components/Faq'

// agent
import BecomeAnAgent from './components/Become-an-agent'
import AgentRegister from './components/agents/Agent-register'
import AgentRegisterVerify from './components/agents/Agent-register-verify'
import Agent from './components/agents/Agent'

import Profile from './components/Profile'

import CreateAccount from './components/Create-account'
import UserDashboard from './components/User-dashboard'
import PendingApplications from './components/Pending-applications'
import Notifications from './components/Notifications'

// shared
import Loader from './components/shared/Loader'
Vue.component('loader', Loader)
import Delivery from './components/policies/shared/Delivery'
Vue.component('delivery', Delivery)
import Checkout from './components/policies/shared/Checkout'
Vue.component('checkout', Checkout)
import Payment from './components/policies/shared/Payment'
Vue.component('payment', Payment)

import Datepicker from 'vue-datepicker'
Vue.component('date-picker', Datepicker)

import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
Vue.component('flat-pickr', flatPickr)

// quotes
import GetFreeQuotes from './components/Get-free-quotes'
import InsuranceTypesInput from './components/inputs/Insurance-types-input'
Vue.component('insurance-types-input', InsuranceTypesInput)
import Quotes from './components/Quotes'
import Quote from './components/quotes/Quote'
Vue.component('quote', Quote)
import OptionalBenefits from './components/quotes/Optional-benefits'
Vue.component('optional-benefits', OptionalBenefits)
import Share from './components/quotes/Share'
Vue.component('share', Share)

// auth
import Login from './components/Login';

// policy profile
import PolicyProfile from './components/Policy-profile'

// travel
import TravelQuotes from './components/policies/travel/Travel-quotes'
Vue.component('travel-quotes', TravelQuotes)
import TravelPrimary from './components/policies/travel/Travel-primary'
Vue.component('travel-primary', TravelPrimary)
import TravelSecondary from './components/policies/travel/Travel-secondary'
Vue.component('travel-secondary', TravelSecondary)

// marine
import MarineQuotes from './components/policies/marine/Marine-quotes'
Vue.component('marine-quotes', MarineQuotes)
import MarinePrimary from './components/policies/marine/Marine-primary'
Vue.component('marine-primary', MarinePrimary)
import MarineSecondary from './components/policies/marine/Marine-secondary'
Vue.component('marine-secondary', MarineSecondary)

// SME business
import SmeQuotes from './components/policies/sme/Sme-quotes'
Vue.component('sme-quotes', SmeQuotes)
import SmePrimary from './components/policies/sme/Sme-primary'
Vue.component('sme-primary', SmePrimary)
import SmeSecondary from './components/policies/sme/Sme-secondary'
Vue.component('sme-secondary', SmeSecondary)

// vehicle
import VehicleQuotes from './components/policies/vehicle/Vehicle-quotes'
Vue.component('vehicle-quotes', VehicleQuotes)
import VehiclePrimary from './components/policies/vehicle/Vehicle-primary'
Vue.component('vehicle-primary', VehiclePrimary)
import VehicleSecondary from './components/policies/vehicle/Vehicle-secondary'
Vue.component('vehicle-secondary', VehicleSecondary)

// golfers
import GolfersQuotes from './components/policies/golfers/Golfers-quotes'
Vue.component('golfers-quotes', GolfersQuotes)
import GolfersPrimary from './components/policies/golfers/Golfers-primary'
Vue.component('golfers-primary', GolfersPrimary)
import GolfersSecondary from './components/policies/golfers/Golfers-secondary'
Vue.component('golfers-secondary', GolfersSecondary)

// home
import HomeQuotes from './components/policies/home/Home-quotes'
Vue.component('home-quotes', HomeQuotes)
import HomePrimary from './components/policies/home/Home-primary'
Vue.component('home-primary', HomePrimary)
import HomeSecondary from './components/policies/home/Home-secondary'
Vue.component('home-secondary', HomeSecondary)

// funeral
import FuneralQuotes from './components/policies/funeral/Funeral-quotes'
Vue.component('funeral-quotes', FuneralQuotes)
import FuneralPrimary from './components/policies/funeral/Funeral-primary'
Vue.component('funeral-primary', FuneralPrimary)
import FuneralSecondary from './components/policies/funeral/Funeral-secondary'
Vue.component('funeral-secondary', FuneralSecondary)

// personal
import PersonalQuotes from './components/policies/personal/Personal-quotes'
Vue.component('personal-quotes', PersonalQuotes)
import PersonalPrimary from './components/policies/personal/Personal-primary'
Vue.component('personal-primary', PersonalPrimary)
import PersonalSecondary from './components/policies/personal/Personal-secondary'
Vue.component('personal-secondary', PersonalSecondary)

// health
import HealthQuotes from './components/policies/health/Health-quotes'
Vue.component('health-quotes', HealthQuotes)
import HealthPrimary from './components/policies/health/Health-primary'
Vue.component('health-primary', HealthPrimary)
import HealthSecondary from './components/policies/health/Health-secondary'
Vue.component('health-secondary', HealthSecondary)
import HealthTertiary from './components/policies/health/Health-tertiary'
Vue.component('health-tertiary', HealthTertiary)

// headers
import MainHeader from './components/layouts/Main-header.vue'
Vue.component('main-header', MainHeader)

import LiaisonHeader from './components/layouts/Liaison-header.vue'
Vue.component('liaison-header', LiaisonHeader)

// active polices
import ActivePolicies from './components/Active-policies'
Vue.component('active-policies', ActivePolicies)
    //Add benefits
    // import AddBenefits from './components/Add-benefits'
    // Vue.component('Add-benefits', AddBenefits)


// upload
import Upload from './components/Upload'
Vue.component('upload', Upload)

//mailing
import Mailings from './components/policies/shared/Mailing'
Vue.component('mailing', Mailings)

//coverage start date
import CoverageStartDate from './components/policies/shared/Coverage-start-date'
Vue.component('coverageStartDate', CoverageStartDate)

// Claims
import SuspendCancel from './components/Suspend-cancel'
Vue.component('Add-benefits', SuspendCancel)

import VehicleAccidentReport from './components/claims/vehicle/Vehicle-accident-report'
Vue.component('Vehicle-accident-report', VehicleAccidentReport)

//define your routes
const routes = [{
        path: '/',
        component: GetFreeQuotes
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/how-it-works',
        component: HowItWorks
    },
    {
        path: '/our-providers',
        component: OurProviders
    },
    {
        path: '/about-us',
        component: AboutUs
    },
    {
        path: '/faq',
        component: Faq
    },

    {
        path: '/become-an-agent',
        component: BecomeAnAgent
    },
    {
        path: '/agents/register',
        component: AgentRegister
    },
    {
        path: '/agents/register/verify',
        component: AgentRegisterVerify
    },
    {
        path: '/agents/agent/:agent_id',
        component: Agent,
        props: true,
        meta: { requiresAuth: true }
    },

    {
        path: '/profile',
        component: Profile,
        meta: { requiresAuth: true }
    },

    {
        path: '/get-free-quotes',
        component: GetFreeQuotes
    },
    {
        path: '/quotes/:id',
        component: Quotes,
        props: true
    },

    {
        path: '/create-account',
        component: CreateAccount,
        meta: { checkAccount: true }
    },
    {
        path: '/user-dashboard',
        component: UserDashboard,
        meta: { requiresAuth: true }
    },
    {
        path: '/pending-applications',
        component: PendingApplications,
        meta: { requiresAuth: true }
    },
    {
        path: '/notifications',
        component: Notifications,
        meta: { requiresAuth: true }
    },

    {
        path: '/policies/:policy_id/profiles/:profile_id/:profile_section',
        component: PolicyProfile,

        props: true,
        meta: { requiresAuth: true }
    },
    { path: '/vehicle-accident-report', component: VehicleAccidentReport },
    { path: '/active-policies', component: ActivePolicies },
]

// Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes, // short for routes: routes
    mode: 'history'
})


//check if route is protected & if user is llogged in
router.beforeEach((to, from, next) => {
    var expires_at = parseInt(localStorage.getItem('expires_at'));
    var token = parseInt(localStorage.getItem('token'));

    var timeStamp = parseInt(Math.floor(Date.now() / 1000));

    if (timeStamp > expires_at) {
        localStorage.removeItem('expires_at');
        localStorage.removeItem('token');
    }

    if (to.meta.requiresAuth) {

        const authUser = window.localStorage.getItem('token');

        if (!authUser) {
            next({ path: '/login' })
        } else {
            next()
        }
    } else if (to.meta.checkAccount) {
        const authUser = window.localStorage.getItem('token');

        if (authUser) {
            next({ path: '/user-dashboard' })
        } else {
            next()
        }
    } else {
        next()
    }
})

//instatinat the vue instance
new Vue({
        //define the selector for the root component
        el: '#app',
        //pass the template to the root component
        template: '<App/>',
        //declare components that the root component can access
        components: {
            App
        },
        //pass in the router to the vue instance
        router,
        store
    }).$mount('#app') //mount the router on the app