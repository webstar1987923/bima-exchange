export default {
    place(state) {
        return state.place;
    },

    driver(state) {
        return state.driver;
    },

    vehicle(state) {
        return state.vehicle;
    },

    agentVerifiedStatus(state) {
        return state.agent_verified_status;
    },

    loading(state) {
        return state.loading;
    },

    //getter for driver
    driver(state) {
        return state.driver;
    },
    user(state) {
        return state.user;
    },

    profiles(state) {
        return state.profiles;
    },

    policies(state) {
        return state.policies;
    },

    agent(state) {
        return state.agent;
    },

    motorInsurance(state) {
        return state.motorInsurance;
    },

    golfersInsurance(state) {
        return state.golfersInsurance;
    },

    homeInsurance(state) {
        return state.homeInsurance;
    },

    funeralInsurance(state) {
        return state.funeralInsurance;
    },

    personalInsurance(state) {
        return state.personalInsurance;
    },

    healthDetails(state) {
        return state.healthDetails;
    },

    healthStatus(state) {
        return state.healthStatus;
    },

    delivery(state) {
        return state.delivery;
    },

    agentPageState(state) {
        return state.agentPageState;
    },

    insuranceTypes(state) {
        return state.insuranceTypes;
    },

    coverTypes(state) {
        return state.coverTypes;
    },

    vehicleUses(state) {
        return state.vehicleUses;
    },

    quote(state) {
        return state.quote;
    },

    quotes(state) {
        return state.quotes;
    },

    optionalBenefits(state) {
        return state.optionalBenefits;
    },

    years(state) {
        return state.years;
    },

    showOptionalBenefitsDiv(state) {
        return state.showOptionalBenefitsDiv;
    },

    checkedOptionalBenefits(state) {
        return state.checkedOptionalBenefits;
    },

    token(state) {
        return state.token;
    },

    biometricsStatus(state) {
        return state.biometrics_status;
    },

    biodata(state) {
        return state.biodata;
    },

    agentUnderwriters(state) {
        return state.agent_underwriters;
    },

    loggedInUser(state) {
        try {
            // return JSON.parse(state.loggedInUser);
            return state.loggedInUser;
        } catch (e) {
            return state.loggedInUser;
        }
    },

    vehicleRegistrationNumbers(state) {
        return state.vehicle_registration_numbers;
    },

    vehicleRegistrationNumberStatus(state) {
        return state.vehicle_registration_number_status;
    },

    countries(state) {
        return state.countries;
    },

    locations(state) {
        return state.locations;
    },

    underwriters(state) {
        return state.underwriters;
    },

    branches(state) {
        return state.branches;
    },

    conditions(state) {
        return state.conditions;
    },

    conditions(state) {
        return state.conditions;
    },

    cargoTypes(state) {
        return state.cargoTypes;
    },

    transportTypes(state) {
        return state.transportTypes;
    },

    packagingTypes(state) {
        return state.packagingTypes;
    },

    currentBenefits(state) {
        return state.currentBenefits;
    },

    providers(state) {
        return state.providers;
    },

    upload(state) {
        return state.upload;
    },

    householdItem(state) {
        return state.household_item;
    },

    householdItems(state) {
        return state.household_items;
    },

    uploadVariable(state) {
        return state.upload_variable;
    }
}