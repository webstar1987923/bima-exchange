export default {
    place: null,
    vehicle: {},
    driver: {},
    verified_status: null,
    agent_underwriters: [],
    biodata: {},
    //drivers license
    driverslicense: {},
    biometrics_status: null,
    loading: false,
    profiles: [{
        agent_code: null,
        country_id: 1,
        insurance_type_id: null,
        insurance_type: {},
        cover_type_id: null,
        cover_type: null,
        coverage_period: 12,
        current_section: null,
        installments: 0,
        ipf_id: null,
        ipf: {},
        provider_id: null,
        provider: {},
        selected_quotes: [],
        drop_off_lat: null,
        drop_off_long: null,
        delivery_datetime: null,

        // vehicle
        vehicle_use_id: null,
        vehicle_use: null,
        sum_insured_on_vehicle: 0,
        vehicle_year_of_manufacture: null,
        vehicle_seating_capacity: null,
        tare_weight: null,
        vehicle_registration_status: null,
        registration_number: null,

        // health
        covers_spouse: 0,
        spouse: false,
        date_of_birth: null,
        gender: null,
        spouse_date_of_birth: null,
        covers_dependents_below_19_years: 0,
        dependents_below_19_years: 0,
        covers_dependents_between_19_25_years: 0,
        dependents_between_19_25_years: 0,

        // funeral
        date_of_birth: null,
        gender: null,
        spouse_date_of_birth: null,
        cover_parents_below_91_years: 0,
        parents_below_91_years: 0,
        cover_parents_in_law_below_91_years: 0,
        parents_in_law_below_91_years: 0,
        cover_limit: 0,

        // travel
        specify_travelers: [],
        adult_travelers: 0,
        dependant_travelers: 0,
        adults: [],
        dependants_below_18_years: [],
        country_travel: "",
        reason_travel: "",
        departure_date: "",
        return_date: "",
        adult_traveller: 0,
        under_18_years: 0,
        country_of_travel_id: null,
        toy_included: false,

        // marine
        cargo_type_id: null,
        cargo_type: {},
        transport_type_id: null,
        transport_type: {},
        packaging_type_id: null,
        packaging_type: {},
        sum_insured_on_marine: 0,
        goods_as_per_invoice: null,
        origin_port: null,
        destination_port: null,
        transshipment: null,
        commercial_invoice_ref_no: 0,
        origin_port_id: null,
        destination_country_id: null,
        expected_shipping_date: null,

        // home
        building: false,
        sum_insured_on_building: null,
        covers_household_items: false,
        sum_insured_on_household_items: null,
        portable_items: false,
        sum_insured_on_portable_items: null,
        insure_domestic_employees: false,
        indoors_employees: null,
        outdoors_employees: null,

        // SME
        sum_on_furniture_fixtures_fittings: null,
        sum_on_stock_for_trade: null,
        sum_on_office_contents: null,
        sum_on_business_interruption_income: null,
        sum_on_business_interruption_wages: null,
        sum_on_business_interruption_auditors_fees: null,
        sum_on_electronic_equipments: null,
        sum_on_electronic_equipments_worldwide: null,
        sum_on_portable_items_worldwide: null,
        sum_on_burglary_declared_value: null,
        sum_on_burglary_first_loss: null,
        sum_on_money_in_transit: null,
        sum_on_money_in_premises_during_business_hours: null,
        sum_on_money_in_safe_outside_business_hours: null,
        sum_on_money_damage_to_safe: null,
        sum_on_money_with_authorized_employees: null,
        sum_on_money_estimated_annual_carry: null,
        sum_on_glass: null,
        sum_on_fidelity_guarantee_one_employee_limit: null,
        sum_on_fidelity_guarantee_aggregate_limit: null,
        sum_on_goods_in_transit_one_vehicle_of_load: null,
        sum_on_goods_in_transit_annual_carry: null,
        sum_on_indemnity_limit: null,
        sum_on_wiba_annual_wages: null,
        sum_on_wiba_clerical_admin_management_stuff: null,
        sum_on_wiba_casual_temporary_others: null,
        sum_on_gpa_annual_wages: null,
        sum_on_gpa_clerical_admin_management_stuff: null,
        sum_on_gpa_casual_temporary_others: null,
        sum_on_machinary_breakdown: null,
        company: null,
        contact_person: null,
        office_location_id: null,
        building_number: null,

        // sme business
        sme_fire_perils: {
            checked: 0,
            show: false,
            office_furniture: false,
            stock_for_trade: false,
            other_office: false
        },

        sme_building: {
            show: false,
            sum_insured_building: 0,
        },

        sme_business_interruption: {
            checked: 0,
            show: false,
            gross_profit: false,
            staff_wages: false,
            auditors_fees: false
        },
        sme_electronic_equipment: {
            checked: 0,
            show: false,
            office_computers: false,
            portable_laptop: false
        },
        sme_portable_office_items: {
            checked: 0,
            show: false,
            value_of_items_carried: 0
        },
        sme_burglary: {
            checked: 0,
            show: false,
            value_insured_event_barglary: 0,
            first_loss: 0
        },
        sme_money: {
            checked: 0,
            show: false,
            cash_in_transit: 0,
            cash_on_premise: 0,
            cash_in_safe: 0,
            damage_to_office: 0,
            cash_with_authorized: 0,
            estimated_cash: 0
        },
        sme_plate_glass: {
            checked: 0,
            show: false,
            glass_items: 0
        },
        sme_fidelity_guarantee: {
            checked: 0,
            show: false,
            fidelity_exposure: 0,
            aggredgated_annual: 0
        },
        sme_goods_in_transit: {
            checked: 0,
            show: false,
            vehicle_load: 0,
            estimated_annual_carry: 0
        },
        sme_public_liability: {
            checked: 0,
            show: false,
            limit_of_indemnity: 0
        },
        sme_wiba: {
            checked: 0,
            show: false,
            admin: 0,
            casual: 0
        },
        sme_group_personal: {
            checked: 0,
            show: false,
            admin: 0,
            casual: 0
        },
        sme_employer_liability: {
            checked: 0,
            show: false,
            option_A: 0,
            option_C: 0
        },
        sme_machine_break: {
            show: false,
            machinery_breakdown: 0
        },
    }],
    policies: [],
    user: {
        kenyan_citizen: true,
        first_name: null,
        middle_name: null,
        last_name: null,
        company_name: null,
        email: null,
        mobile_number: null,
        postal_address: null,
        postal_code: null,
        location: {},
        location_id: null,
        city: null,
        agent_status: null,
        national_id: null,
        alien_id: null,
        pin: null,
        IRA_registration_number: null,
        AKI_certificate_serial: null,
        password: null,
        password_confirmation: null,
        registered_agent: 0,
        agent_status_details: null,
        underwriters: {},
    },
    quotes: [],
    insuranceTypes: [],
    coverTypes: [],
    optionalBenefits: [],
    vehicleUses: [],
    years: [],
    quote: {},
    showOptionalBenefitsDiv: false,
    checkedOptionalBenefits: [],
    token: localStorage.getItem('token'),
    loggedInUser: null,
    agentPageState: '',
    vehicle_registration_numbers: [],
    vehicle_registration_number_status: '',
    locations: [],
    underwriters: [],
    branches: [],
    conditions: [
        'high_blood_pressure',
        'heart_disease',
        'high_cholesterol',
        'asthma',
        'chronic_obstructive_airway_disease',
        'sinus_disease',
        'thyroid_disease',
        'diabetes_mellitus',
        'paralysis',
        'epilepsy',
        'sickle_cell_disease',
        'leukemia',
        'arthritis',
        'gout',
        'chronic_back_pain_slipped_disc',
        'pelvic_inflammatory_disease',
        'fibroids',
        'enlargement_of_the_prostate',
        'liver_disease',
        'stomach_duodenal_ulcers',
        'surgical_operation',
        'hospitalized',
        'regular_medication',
        'history_of_caesarian',
    ],
    isLoggedIn: !!localStorage.getItem('token'),
    cargoTypes: [],
    transportTypes: [],
    packagingTypes: [],
    countries: [],
    agent_verified_status: null,
    currentBenefits: [],
    providers: [],
    upload: {
        policy_id: null,
        profie_id: null,
        file_name: null,
        file: null
    },
    household_item: {
        item: null,
        value: null
    },
    household_items: [],

    //state for the driver
    driver: {},
    household_items: [],
    upload_variable: null
}