import Vue from 'vue';

import Vuex from 'vuex';
Vue.use(Vuex);

import lodash from 'lodash'

import axios from 'axios'
const HTTP = axios.create({
    // baseURL: `http://liaison.ebima.test/index.php`
    baseURL: `https://07f9cea4.ngrok.io/`

})

HTTP.interceptors.request.use(
    config => {
        config.headers.authorization = `Bearer ${window.localStorage.getItem('token')}`
        return config;
    },

    error => Promise.reject(error)
)

export default {
    getLoggedInUser({
        commit,
        state
    }) {
        return HTTP.get('api/user').then(function (response) {
            if (response.status === 200) {
                // console.log(response.data);
                commit('setLoggedInUser', response.data);
                return response.data;
            }
        }, (err) => {

        });
    },

    getUnderwriters({
        commit,
        state
    }) {
        HTTP.get('api/providers/underwriters').then(function (response) {
            if (response.status === 200) {
                let underwriters = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        underwriters.push(response.data[key]);
                    }
                }

                commit('setUnderwriters', underwriters);
            }
        }, (response) => {

        });
    },

    getCountries({
        commit,
        state
    }) {
        HTTP.get('api/countries').then(function (response) {
            if (response.status === 200) {
                let countries = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        countries.push(response.data[key]);
                    }
                }

                commit('setCountries', countries);
            }
        }, (response) => {

        });
    },

    getLocations({
        commit,
        state
    }) {
        HTTP.get('api/locations').then(function (response) {
            if (response.status === 200) {
                let locations = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        locations.push(response.data[key]);
                    }
                }

                commit('setLocations', locations);
            }
        }, (response) => {

        });
    },


    getInsuranceTypes({
        commit,
        state
    }) {
        commit('setLoading', true);

        HTTP.get('api/insurance_types').then(function (response) {
            if (response.status === 200) {
                let insuranceTypes = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        insuranceTypes.push(response.data[key]);
                    }
                }

                commit('setInsuranceTypes', insuranceTypes);
                commit('setLoading', false);
            }
        }, (response) => {

        });
    },

    getCoverTypes({
        commit,
        state
    }) {
        HTTP.get('api/insurance_types/2/cover_types').then(function (response) {
            if (response.status === 200) {
                let coverTypes = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        coverTypes.push(response.data[key]);
                    }
                }

                commit('setCoverTypes', coverTypes);
            }
        }, (response) => {

        });
    },

    getVehicleUses({
        commit,
        state
    }) {
        HTTP.get('api/vehicle_uses').then(function (response) {
            if (response.status === 200) {
                let vehicleUses = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        vehicleUses.push(response.data[key]);
                    }
                }

                commit('setVehicleUses', vehicleUses);
            }
        }, (response) => {

        });
    },

    generateQuote({
        commit,
        state
    }) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/store', {
            profile: state.profile
        }).then(function (response) {
            if (response.status === 200) {
                commit('setLoading', false);

                return response.data;
            }
        }, (response) => {

        });
    },

    getQuote({
        dispatch,
        commit,
        state
    }, id) {
        return HTTP.get('api/quotes/quote?quote_id=' + id).then(function (response) {
            if (response.status === 200) {
                return response;
            }
        }, (response) => {

        });
    },

    getQuotes({
        commit,
        state
    }, profile_id) {
        return HTTP.get('api/quotes?profile_id=' + profile_id).then(function (response) {
            if (response.status === 200) {
                return response;
            }
        }, (response) => {

        });
    },

    getBiodata({
        commit,
        state,
        rootState
    }, nationalId) {
        commit('setLoading', true);

        return HTTP.get('api/ndi/biodata?national_id=' + nationalId).then((response) => {
            commit('setLoading', false);
            commit('setBiodata', response.data[0]);

            return response.data;
        }, (err) => {
            console.log("error : " + err);
            commit('setLoading', false);
        });
    },

    register({
        commit,
        state,
        rootState
    }, credentials) {
        commit('setLoading', true);

        return HTTP.post('api/register', credentials).then((response) => {
            if (response.status === 200) {
                localStorage.setItem('token', response.data.access_token);
                commit('setToken', response.data.access_token);

                commit('setLoading', false);
                return response;
            }
        }, (err) => {
            commit('setLoading', false);
            return err.response;
        });
    },

    login({
        commit,
        state,
        rootState
    }, credentials) {
        commit('setLoading', true);

        return HTTP.post('api/login', credentials).then((response) => {
            if (response.status === 200) {
                commit("setLoading", false);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    refreshToken({
        commit,
        state,
        rootState
    }) {
        commit('setLoading', true);

        console.log('refreshToken');

        return HTTP.post('api/login/refresh', {
            headers: {
                Authorization: `Bearer ${window.localStorage.getItem('token')}`
            }
        }).then((response) => {
            if (response.status === 200) {
                commit("setLoading", false);

                // localStorage.setItem("token", response.data.access_token);
                // localStorage.setItem("expires_at", parseInt(Math.floor(Date.now() / 1000)) + parseInt(response.data.expires_in));
                // localStorage.setItem("expires_at", parseInt(Math.floor(Date.now() / 1000)) + 30);
                // this.$store.commit("setToken", response.data.access_token);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },


    logout({
        commit,
        state,
        rootState
    }) {
        commit('setLoading', true);

        return HTTP.post('api/logout', {
            headers: {
                Authorization: `Bearer ${window.localStorage.getItem('token')}`
            }
        }).then((response) => {
            if (response.status === 200) {
                commit("setLoading", false);

                localStorage.removeItem("token");
                commit("setToken", null);
                localStorage.removeItem("logged_in_user");
                commit("setLoggedInUser", null);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    postPolicySync({
        commit,
        state
    }, policies) {
        commit('setLoading', true);

        return HTTP.post('api/policies/sync', {
            data: {
                policies: policies ? policies : state.policies
            }
        }).then((response) => {
            if (response.status === 200) {
                commit('setLoading', false);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    getPolicy({
        commit,
        state
    }, policy) {
        commit('setLoading', true);

        return HTTP.post('api/policies/policy', {
            data: {
                policy: policy
            }
        }).then((response) => {
            if (response.status === 200) {
                commit('setLoading', false);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    postPolicyPayment({
        commit,
        state
    }) {
        commit('updatePolicyProfile', {
            policyIndex: 0,
            profileIndex: 0,
            field: 'payment_method',
            value: 'm-pesa'
        })

        commit('setLoading', true);

        return HTTP.post('api/policies/payment', {
            data: {
                policies: state.policies
            }
        }).then((response) => {
            commit('setLoading', false);

            return response;
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    getProviderBranches({
        commit,
        state
    }, provider) {
        commit('setLoading', true);

        return HTTP.post('api/providers/branches', {
            data: {
                provider: provider
            }
        }).then((response) => {
            if (response.status === 200) {
                commit('setLoading', false);

                return response;
            }
        }, (err) => {
            commit('setLoading', false);

            console.log("error : " + err);
        });
    },

    getVehicleRegistrationNumbers({
        commit,
        state,
        rootState
    }, personalIdentificationNumber) {
        commit('setLoading', true);

        return HTTP.get('api/ndi/vehicle-registration-numbers?kra_pin=' + personalIdentificationNumber).then((response) => {
            commit('setLoading', false);

            return response.data;
        }, (err) => {
            console.log("error : " + err);
        });
    },

    getVehicle({
        commit,
        state,
        rootState
    }, vehicleRegistrationNumber) {
        commit('setLoading', true);

        return HTTP.get('api/ndi/vehicle?vehicle_registration_number=' + vehicleRegistrationNumber).then((response) => {
            commit('setLoading', false);

            return response.data;
        }, (err) => {
            console.log("error : " + err);
        });
    },

    // Quotes
    postQuotesStore({
        commit,
        state
    }) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/store', {
            data: {
                profiles: state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuote({
        commit,
        state
    }, id) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/quote', {
            data: {
                quote: {
                    id
                }
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesProviders({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/providers', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotes({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/quotes', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesIPFs({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/ipfs', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesPaymentPlans({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/payment-plans', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesBenefits({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/benefits', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getOptionalBenefits({
        commit,
        state
    }, insuranceTypeId) {
        commit('setLoading', true);

        return HTTP.get('api/insurance_types/' + insuranceTypeId + '/cover_types/optional_benefits').then((response) => {
            commit('setLoading', false);

            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesOptionalBenefits({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/optional-benefits', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesServiceProviders({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/service-providers', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },

    getQuotesCoverLimits({
        commit,
        state
    }, profiles = null) {
        commit('setLoading', true);

        return HTTP.post('api/quotes/cover-limits', {
            data: {
                profiles: profiles ? profiles : state.profiles
            }

        }).then((response) => {
            commit('setLoading', false);
            return response;

        }, (err) => {
            commit('setLoading', false);
            console.log("error : " + err);
        });
    },




    suspendPolicy({
        commit,
        state
    }, data) {
        commit('setLoading', true);

        return HTTP.patch('api/policies/' + data.id + '/suspend', {
            id: data.id,
            suspend: data.data
        }).then((response) => {
            commit('setLoading', false);
            return response.data;
        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },

    cancelPolicy({
        commit,
        state
    }, data) {
        commit('setLoading', true);

        return HTTP.patch('api/policies/' + data.id + '/cancel-credit', {
            id: data.id,
            cancel: data.data
        }).then((response) => {
            commit('setLoading', false);
            return response.data;
        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },

    registerAgent({
        commit,
        state,
        rootState
    }, data) {
        commit('setLoading', true);

        return HTTP.post('api/agents/register', data).then((response) => {
            localStorage.setItem('logged_in_user', JSON.stringify(response.data));
            commit('setLoggedInUser', response.data);

            localStorage.setItem('token', response.data.api_token);
            commit('setToken', response.data.api_token);

            commit('setLoading', false);

            return response.data;
        }, (err) => {
            return err;
        });
    },

    getAgent({
        dispatch,
        commit,
        state
    }, id) {
        HTTP.get('api/agents/' + id).then(function (response) {
            if (response.status === 200) {

                try {
                    commit('setUser', JSON.parse(response.data.agent));
                } catch (e) {
                    commit('setUser', response.data.agent);
                }
            }
        }, (response) => {

        });
    },

    getPolicies({
        dispatch,
        commit,
        state
    }, id) {
        HTTP.get('api/policies').then(function (response) {
            if (response.status === 200) {
                commit('setPolicies', response.data.policies);
            }
        }, (response) => {

        });
    },

    getPendingPolicies({
        commit,
        state
    }) {
        return HTTP.get('api/policies/pending').then((response) => {
            if (response.status === 200) {

                return response;
            }
        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },

    getActivePolicies({
        commit,
        state
    }) {
        return HTTP.get('api/policies/active', {
            headers: {
                Authorization: `${window.localStorage.getItem('token')}`
            }
        }).then((response) => {
            if (response.status === 200) {
                commit('setActivePolicies', response.data.data);
            }
        }, (err) => {
            console.log("error : " + err);
        });
    },

    postAddBenefits({
        commit,
        state,
    }, policy) {
        return HTTP.post('api/policies/' + policy.id + '/optional-benefits').then((response) => {
            if (response.status === 200) {
                commit('setProfile', response.data);

                return response;
            }
        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },
    getOptionalBenefitsOffers({
        commit,
        state,
    }, id) {
        commit('setLoading', true);
        return HTTP.post('api/policies/' + id + '/optional-benefits').then((response) => {
            if (response.status === 200) {
                /*commit('setProfile', response.data);*/
                return response;
            }
        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },

    getCargoTypes({
        commit,
        state
    }) {
        HTTP.get('api/cargo_types/active').then(function (response) {
            if (response.status === 200) {
                let cargoTypes = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        cargoTypes.push(response.data[key]);
                    }
                }

                commit('setCargoTypes', cargoTypes);
            }
        }, (response) => {

        });
    },

    getTransportTypes({
        commit,
        state
    }) {
        HTTP.get('api/transport_types/active').then(function (response) {
            if (response.status === 200) {
                let transportTypes = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        transportTypes.push(response.data[key]);
                    }
                }

                commit('setTransportTypes', transportTypes);
            }
        }, (response) => {

        });
    },

    getPackagingTypes({
        commit,
        state
    }) {
        HTTP.get('api/packaging_types/active').then(function (response) {
            if (response.status === 200) {
                let packagingTypes = [];

                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        packagingTypes.push(response.data[key]);
                    }
                }

                commit('setPackagingTypes', packagingTypes);
            }
        }, (response) => {

        });
    },

    postFiles({
        commit,
        state
    }, data) {
        commit('setLoading', true);
        var formData = new FormData()
        formData.append('policy_id', data.policy_id)
        formData.append('profile_id', data.profile_id)
        formData.append('file_name', data.file_name)
        formData.append('file', data.file)

        return HTTP.post('api/policies/upload', formData).then((response) => {
            commit('setLoading', false);

        }, (err) => {
            console.log("error : " + err.bodyText);
        });
    },

    postHouseholdItems({
        commit,
        state
    }, data) {
        return HTTP.post('api/policies/' + state.policies[0].id + '/household-item', data).then((response) => {


            // commit('setHouseholdItem', null);
            // commit('setHouseholdItems', response.data);
            console.log('worked')

            // if (response.status === 200) {
            //     commit('setProfile', response.data);

            //     return response;
            // }
        }, (err) => {
            console.log("error : " + err.bodyText);
        });

    },

    getCityName({
        commit,
        state
    }, postal_code) {
        return HTTP.get(`api/cities/verify/${postal_code}`).then(function (response) {
            if (response.status === 200) {
                return response;
            }
        }, (err) => {

        });
    },

    getKRAPINByNationalID({
        commit,
        state,
        rootState
    }, nationalId) {
        commit('setLoading', true);

        return HTTP.get('api/ndi/kra-pin/national-id?national_id=' + nationalId).then((response) => {
            commit('setLoading', false);

            return response;
        }, (err) => {
            console.log("error : " + err);
            commit('setLoading', false);
        });
    },

    getVehicleByKRAPINAndNationalId({
        commit,
        state,
        rootState
    }, {
        KRAPIN,
            vehicleRegistrationNumber
    }) {
        commit('setLoading', true);

        return HTTP.get(`api/ndi/vehicle/kra_pin-registration_number?kra_pin=${KRAPIN}&registration_number=${vehicleRegistrationNumber}`).then((response) => {
            commit('setLoading', false);

            return response;
        }, (err) => {
            console.log("error : " + err);
            commit('setLoading', false);
        });
    },

    getVehicleInsured({
        commit,
        state
    }, registration_number) {
        return HTTP.get(`api/policies/active/vehicle/${registration_number}`).then(function (response) {
            if (response.status === 200) {
                return response;
            }
        }, (err) => {

        });
    },

    postClaimsSync({
        commit,
        state
    }, claim) {
        return HTTP.post(`api/claims/sync`, claim).then(function (response) {
            return response;
            if (response.status === 200) {
            }
        }, (err) => {

        });
    },

    //DriversLicense
    getDriversLicense({
        commit,
        state,
        rootState
    }, nationalId) {
        commit('setLoading', true);

        return HTTP.get('api/ndi/driverslicense?national_id=' + nationalId).then((response) => {
            commit('setLoading', false);
            // commit(' setDriversLicense', response.data[0]);

            return response;
        }, (err) => {
            console.log("error : " + err);
            commit('setLoading', false);
        });
    },

    postClaimupload({
        commit,
        state
    }, upload_data) {
        return HTTP.post(`api/claims/upload`, upload_data).then(function (response) {
            //return response;
            if (response.status === 200) {
                return response;
            }
        }, (err) => {

        });
    },
}